# Brazilian Portuguese translation of glib-networking.
# Copyright (C) 2017 glib-networking's COPYRIGHT HOLDER
# This file is distributed under the same license as the glib-networking package.
# André Gondim <In memoriam>, 2011.
# Djavan Fagundes <djavan@comum.org>, 2011.
# Jonh Wendell <jwendell@gnome.org>, 2012.
# Rafael Fontenelle <rafaelff@gnome.org>, 2012, 2017, 2018.
msgid ""
msgstr ""
"Project-Id-Version: glib-networking master\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?"
"product=glib&keywords=I18N+L10N&component=network\n"
"POT-Creation-Date: 2018-02-08 06:15+0000\n"
"PO-Revision-Date: 2018-02-01 05:19-0200\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"
"X-Project-Style: gnome\n"

#: proxy/libproxy/glibproxyresolver.c:159
msgid "Proxy resolver internal error."
msgstr "Erro interno do resolvedor de proxy."

#: tls/gnutls/gtlscertificate-gnutls.c:182
#, c-format
msgid "Could not parse DER certificate: %s"
msgstr "Não foi possível analisar certificado DER: %s"

#: tls/gnutls/gtlscertificate-gnutls.c:203
#, c-format
msgid "Could not parse PEM certificate: %s"
msgstr "Não foi possível analisar certificado PEM: %s"

#: tls/gnutls/gtlscertificate-gnutls.c:234
#, c-format
msgid "Could not parse DER private key: %s"
msgstr "Não foi possível analisar chave privada DER: %s"

#: tls/gnutls/gtlscertificate-gnutls.c:265
#, c-format
msgid "Could not parse PEM private key: %s"
msgstr "Não foi possível analisar chave privada PEM: %s"

#: tls/gnutls/gtlscertificate-gnutls.c:304
msgid "No certificate data provided"
msgstr "Nenhum dado de certificado fornecido"

#: tls/gnutls/gtlsclientconnection-gnutls.c:398
msgid "Server required TLS certificate"
msgstr "O servidor requer certificado TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:392
#, c-format
msgid "Could not create TLS connection: %s"
msgstr "Não foi possível criar conexão TLS: %s"

#: tls/gnutls/gtlsconnection-gnutls.c:697
msgid "Connection is closed"
msgstr "A conexão está encerrada"

#: tls/gnutls/gtlsconnection-gnutls.c:772
#: tls/gnutls/gtlsconnection-gnutls.c:2184
msgid "Operation would block"
msgstr "A operação bloquearia"

#: tls/gnutls/gtlsconnection-gnutls.c:813
#: tls/gnutls/gtlsconnection-gnutls.c:1400
msgid "Socket I/O timed out"
msgstr "Tempo de E/S do soquete foi esgotado"

#: tls/gnutls/gtlsconnection-gnutls.c:952
#: tls/gnutls/gtlsconnection-gnutls.c:985
msgid "Peer failed to perform TLS handshake"
msgstr "Peer falhou ao realizar negociação TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:970
msgid "Peer requested illegal TLS rehandshake"
msgstr "O peer requisitou uma negociação TLS ilegal"

#: tls/gnutls/gtlsconnection-gnutls.c:991
msgid "TLS connection closed unexpectedly"
msgstr "Conexão TLS fechou inesperadamente"

#: tls/gnutls/gtlsconnection-gnutls.c:1001
msgid "TLS connection peer did not send a certificate"
msgstr "Conexão TLS não enviou um certificado"

#: tls/gnutls/gtlsconnection-gnutls.c:1007
#, c-format
msgid "Peer sent fatal TLS alert: %s"
msgstr "O peer enviou alerta TLS fatal: %s"

#: tls/gnutls/gtlsconnection-gnutls.c:1015
#, c-format
msgid "Message is too large for DTLS connection; maximum is %u byte"
msgid_plural "Message is too large for DTLS connection; maximum is %u bytes"
msgstr[0] "A mensagem é grande demais para conexão DTLS; máximo é %u byte"
msgstr[1] "A mensagem é grande demais para conexão DTLS; máximo é %u bytes"

#: tls/gnutls/gtlsconnection-gnutls.c:1022
msgid "The operation timed out"
msgstr "Tempo da operação foi esgotado"

#: tls/gnutls/gtlsconnection-gnutls.c:1808
#: tls/gnutls/gtlsconnection-gnutls.c:1859
msgid "Error performing TLS handshake"
msgstr "Erro executando negociação TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:1869
msgid "Server did not return a valid TLS certificate"
msgstr "Servidor não retornou certificado TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:1946
msgid "Unacceptable TLS certificate"
msgstr "Certificado TLS inaceitável"

#: tls/gnutls/gtlsconnection-gnutls.c:2218
#: tls/gnutls/gtlsconnection-gnutls.c:2310
msgid "Error reading data from TLS socket"
msgstr "Erro ao ler dados do socket TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:2340
#, c-format
msgid "Receive flags are not supported"
msgstr "Não há suporte a recebimento de sinalizadores"

#. flags
#: tls/gnutls/gtlsconnection-gnutls.c:2417
#: tls/gnutls/gtlsconnection-gnutls.c:2489
msgid "Error writing data to TLS socket"
msgstr "Erro ao gravar dados do socket TLS"

#: tls/gnutls/gtlsconnection-gnutls.c:2459
#, c-format
msgid "Message of size %lu byte is too large for DTLS connection"
msgid_plural "Message of size %lu bytes is too large for DTLS connection"
msgstr[0] ""
"Uma mensagem de tamanho %lu byte é grande demais para uma conexão DTLS"
msgstr[1] ""
"Uma mensagem de tamanho %lu bytes é grande demais para uma conexão DTLS"

#: tls/gnutls/gtlsconnection-gnutls.c:2461
#, c-format
msgid "(maximum is %u byte)"
msgid_plural "(maximum is %u bytes)"
msgstr[0] "(máximo é %u byte)"
msgstr[1] "(máximo é %u bytes)"

#: tls/gnutls/gtlsconnection-gnutls.c:2520
#, c-format
msgid "Send flags are not supported"
msgstr "Não há suporte a envio de sinalizadores"

#: tls/gnutls/gtlsconnection-gnutls.c:2623
msgid "Error performing TLS close"
msgstr "Erro ao executar fechamento TLS"

#: tls/gnutls/gtlsserverconnection-gnutls.c:111
msgid "Certificate has no private key"
msgstr "O certificado não contém nenhuma chave privada"

#: tls/pkcs11/gpkcs11pin.c:111
msgid ""
"This is the last chance to enter the PIN correctly before the token is "
"locked."
msgstr ""
"Esta é a última chance de digitar o PIN corretamente antes que o token seja "
"bloqueado."

#: tls/pkcs11/gpkcs11pin.c:113
msgid ""
"Several PIN attempts have been incorrect, and the token will be locked after "
"further failures."
msgstr ""
"O PIN foi digitado várias vezes incorretamente, por isso o token será "
"bloqueado agora."

#: tls/pkcs11/gpkcs11pin.c:115
msgid "The PIN entered is incorrect."
msgstr "O PIN digitado está incorreto."

#: tls/pkcs11/gpkcs11slot.c:447
msgid "Module"
msgstr "Módulo"

#: tls/pkcs11/gpkcs11slot.c:448
msgid "PKCS#11 Module Pointer"
msgstr "PKCS#11 Module Pointer"

#: tls/pkcs11/gpkcs11slot.c:455
msgid "Slot ID"
msgstr "Slot ID"

#: tls/pkcs11/gpkcs11slot.c:456
msgid "PKCS#11 Slot Identifier"
msgstr "PKCS#11 Slot Identifier"

#~ msgid "Connection is already closed"
#~ msgstr "A conexão já está encerrada"
